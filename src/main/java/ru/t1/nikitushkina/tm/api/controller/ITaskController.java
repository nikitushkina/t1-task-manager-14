package ru.t1.nikitushkina.tm.api.controller;

public interface ITaskController {

    void createTask();

    void showTasks();

    void clearTask();

    void removeTaskById();

    void removeTaskByIndex();

    void showTaskById();

    void showTaskByIndex();

    void updateTaskById();

    void showTaskByProjectId();

    void updateTaskByIndex();

    void startTaskById();

    void startTaskByIndex();

    void competeTaskById();

    void completeTaskByIndex();

    void changeTaskStatusById();

    void changeTaskStatusByIndex();

}
