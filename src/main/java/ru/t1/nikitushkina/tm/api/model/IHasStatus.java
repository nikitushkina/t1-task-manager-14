package ru.t1.nikitushkina.tm.api.model;

import ru.t1.nikitushkina.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}